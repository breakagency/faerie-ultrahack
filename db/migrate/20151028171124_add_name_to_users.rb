class AddNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :country, :string
    #add_column :users, :is_female, :boolean, default: false
  end
end
