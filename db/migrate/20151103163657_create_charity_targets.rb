class CreateCharityTargets < ActiveRecord::Migration
  def change
    create_table :charity_targets do |t|
      t.integer :user_id
      t.string :type
      t.string :name
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
