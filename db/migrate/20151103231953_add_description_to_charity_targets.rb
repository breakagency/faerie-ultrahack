class AddDescriptionToCharityTargets < ActiveRecord::Migration
  def change
    add_column :charity_targets, :description, :string
  end
end
