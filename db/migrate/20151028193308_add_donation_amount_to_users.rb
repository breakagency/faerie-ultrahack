class AddDonationAmountToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :total_donated, :integer
  end
end
