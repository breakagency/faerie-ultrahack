Rails.application.routes.draw do
  resources :charity_targets do 
    get 'embed'
  end

  resources :hospitals, :controller => "charity_targets", :type => "Hospital"
  resources :schools, :controller => "charity_targets", :type => "School"
  resources :farmlands, :controller => "charity_targets", :type => "Farmland"

  mount Wupee::Engine, at: "/wupee"

  resources :badges
  
  devise_for :users, controllers: { registrations: 'users/registrations' }
  get 'catalogue/index'
  get 'refresh_karma/(:paystairs_id)' => 'catalogue#refresh_karma', as: :refresh_karma

  get 'set_avatar' => 'catalogue#set_avatar', as: :choose_avatar
  get 'set_avatar/:id' => 'catalogue#set_avatar', as: :set_avatar

  root 'catalogue#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
