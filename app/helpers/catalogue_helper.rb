module CatalogueHelper
  	def avatar
  		if current_user.avatar_type
  			asset_path("avatars/" + current_user.avatar_type + ".png")
  		end
  	end

  	def country(user)
  		"from " + user.country if user and user.country
  	end
end
