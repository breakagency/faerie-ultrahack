json.array!(@charity_targets) do |charity_target|
  json.extract! charity_target, :id, :user_id, :type, :name, :latitude, :longitude
  json.url charity_target_url(charity_target, format: :json)
end
