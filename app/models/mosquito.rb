class Mosquito < CharityTarget
	def level1
		"malaria/level_one.jpg"
	end

	def level2
		"malaria/level_two.jpg"
	end

	def level3
		"malaria/level_three.jpg"
	end

	def background
		"hospital/hospital.png"
	end

	def mosquito
		"malaria/normal_mosquito.png"
	end

	def normal_mosquito
		"malaria/normal_mosquito.png"
	end

	def fast_mosquito
		"malaria/fast_mosquito.png"
	end

	def fat_mosquito
		"malaria/fat_and_slow_mosquito.png"
	end

	def dead_mosquito
		"malaria/dead_mosquito.png"
	end

	def dead_fast_mosquito
		"malaria/dead_fat_mosquito.png"
	end

	def hand
		"malaria/hand_icon.png"
	end

	def stick
		"malaria/stick_icon.png"
	end

	def spray
		"malaria/spray_icon.png"
	end

	def net
		"malaria/net_icon.png"
	end

	def you
		"malaria/you.png"
	end

	def lat
		3.867245
	end

	def lng
		22.206180
	end
end