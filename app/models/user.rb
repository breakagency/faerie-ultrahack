require 'wupee'

class User < ActiveRecord::Base
  has_merit

  include Wupee::Receiver

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :badges
  has_many :charity_targets

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  def refresh_karma
    return unless paystairs_id
    res = HTTParty.get("https://paystairs.com/api/1/donations?senderId=" + paystairs_id.to_s, :verify => false )
    return unless res["total"]
    paystairs_total_donated = res["total"].to_i
    if self.total_donated == nil 
      self.total_donated = 0
    end

    if self.total_donated < paystairs_total_donated
      new_donations = paystairs_total_donated - self.total_donated

      self.total_donated += new_donations
      self.add_points(20 * new_donations)
    end

    if self.points >= exp_for(level + 1) 
      byebug
      level_up
      #Pusher.trigger(self.channel, 'level_up', { message: 'Congratulations! You just reached level ' + self.level.to_s})
    end

    self.save
  end

  def level_up
    self.level += 1
    level = self.level
    user = self
    begin
      Wupee.notify do
         attached_object user
         notif_type :level_up # you can also pass an instance of a Wupee::NotificationType class to this method
         subject_vars level: Proc.new { |notification| notification.attached_object.level } # variables to be interpolated the fill in the subject of the email (obviously optional)
         receivers user
         deliver :now # you can overwrite global configuration here, optional
      end
    rescue => error

    end

    self.save
  end

  def channel
    'user-' + paystairs_id
  end

  def exp_for(level)
    case level
    when 0
      0
    when 1
      100
    when 2
      300
    when 3
      1000
    else
      points + 500
    end
  end

  def level_exp
  	points - exp_for(level)
  end

  def next_level
  	exp_for(level + 1)
  end
end
