class CharityTarget < ActiveRecord::Base
	belongs_to :user

	def intro
		"Welcome to the farm!"
	end

	def background
		"none"
	end

	class << self

    def inherited(child)
      child.instance_eval do
        def model_name
          CharityTarget.model_name
        end
      end
      super 
    end
	end
end
