class School < CharityTarget
	def background
		"school/school.png"
	end

	def lat
		21.070987
	end

	def lng
		77.560879
	end
end