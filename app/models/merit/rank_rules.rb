# Be sure to restart your server when you modify this file.
#
# 5 stars is a common ranking use case. They are not given at specified
# actions like badges, you should define a cron job to test if ranks are to be
# granted.
#
# +set_rank+ accepts:
# * :+level+ ranking level (greater is better)
# * :+to+ model or scope to check if new rankings apply
# * :+level_name+ attribute name (default is empty and results in 'level'
#   attribute, if set it's appended like 'level_#{level_name}')

require 'pusher'

module Merit
  class RankRules
    include Merit::RankRulesMethods

    def exp_for(level)
      case level
      when 1
        100
      when 2
        300
      when 3
        1000
      end
    end

    LEVEL_1 = 100
    LEVEL_2 = 300
    LEVEL_3 = 1000

  end
end
