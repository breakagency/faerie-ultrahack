#
# +grant_on+ accepts:
# * Nothing (always grants)
# * A block which evaluates to boolean (recieves the object as parameter)
# * A block with a hash composed of methods to run on the target object with
#   expected values (+votes: 5+ for instance).
#
# +grant_on+ can have a +:to+ method name, which called over the target object
# should retrieve the object to badge (could be +:user+, +:self+, +:follower+,
# etc). If it's not defined merit will apply the badge to the user who
# triggered the action (:action_user by default). If it's :itself, it badges
# the created object (new user for instance).
#
# The :temporary option indicates that if the condition doesn't hold but the
# badge is granted, then it's removed. It's false by default (badges are kept
# forever).

module Merit
  class BadgeRules
    include Merit::BadgeRulesMethods

    def initialize
      # BADGE TYPES
      # Which API methods we need from PayStairs to get correct ones

      # How does the PayStairs internals have to be structured 
      # to allow for condition "donated to 3 different recipients"

      # Donated 3 times to a single recipient
      # Donated 5 times to a single
      # Donated 10 times to a single recipient

      # Donated 3..10 to schools
      # Donated 10..30..100 to schools
      # Donated 100..300..1000 to schools

      # Donated 3..1000 to farmers

      # Donated 3..1000 to medicine

      # Donated to 3 projects in Asia

      # Donated to 3 projects in SouthAmerica

      # Donated to one project in all continents

      # Donated to a whole family
    end
  end
end