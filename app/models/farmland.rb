class Farmland < CharityTarget
	def lat
		-6.208763
	end

	def lng
		106.845599
	end
	
	def background
		"irrigation/irrigation.png"
	end

	def ground
		"farmland/farmlandground.jpg"
	end

	def weed1
		"farmland/badgrass.png"
	end

	def weed2
		"farmland/badgrass2.png"
	end

	def fruit1
		"farmland/apple.png"
	end

	def fruit2
		"farmland/potatoes.png"
	end
end