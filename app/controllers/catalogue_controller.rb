class CatalogueController < ApplicationController
  protect_from_forgery except: [:render_user, :refresh_karma]
  def index
  	@users = User.all
    @notifications = current_user.notifications if current_user
    @charity_targets = current_user.charity_targets if current_user
  end

  #include Merit::RankRulesMethods

  before_action :set_total_donated

  def set_total_donated
  	if current_user and params[:total_donated]
  		current_user.total_donated = params[:total_donated]
  	end
  end

  def refresh_karma
    paystairs_id = params[:paystairs_id]
    return unless paystairs_id

    user = User.find_by_paystairs_id(paystairs_id)
    if not user
      return unless current_user
      return if current_user.paystairs_id

      current_user.paystairs_id = paystairs_id
      current_user.save
      user = current_user
    end

    user.refresh_karma
    current_user.reload
    @notifications = current_user.notifications

    render "render_user.js.erb"
  end

  def set_avatar
  	redirect_to root_url and return if not current_user 

  	if params[:id]
  		avatar = params[:id]
  		current_user.avatar_type = avatar
  		current_user.save
  		flash[:notice] = "You successfully chose your avatar. Awesome!"
  		redirect_to root_url and return
  	end

  	render 'choose'
  end
end