class CharityTargetsController < ApplicationController
  before_action :set_charity_target, only: [:show, :edit, :update, :destroy]

  # GET /charity_targets
  # GET /charity_targets.json
  def index
    @charity_targets = CharityTarget.all
  end

  # GET /charity_targets/1
  # GET /charity_targets/1.json
  def show
    @game = @charity_target
    @notifications = current_user.notifications if current_user
  end

  def embed
    return unless params[:charity_target_id]
    @charity_target = CharityTarget.find(params[:charity_target_id])
    #@image = asset_url(@charity_target.type || "farmland" + ".png")
    @game = @charity_target
    render "embed", :layout => false 
  end

  # GET /charity_targets/new
  def new
    @charity_target = CharityTarget.new
  end

  # GET /charity_targets/1/edit
  def edit
  end

  # POST /charity_targets
  # POST /charity_targets.json
  def create
    @charity_target = CharityTarget.new(charity_target_params)

    respond_to do |format|
      if @charity_target.save
        format.html { redirect_to @charity_target, notice: 'Charity target was successfully created.' }
        format.json { render :show, status: :created, location: @charity_target }
      else
        format.html { render :new }
        format.json { render json: @charity_target.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /charity_targets/1
  # PATCH/PUT /charity_targets/1.json
  def update
    respond_to do |format|
      if @charity_target.update(charity_target_params)
        format.html { redirect_to @charity_target, notice: 'Charity target was successfully updated.' }
        format.json { render :show, status: :ok, location: @charity_target }
      else
        format.html { render :edit }
        format.json { render json: @charity_target.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /charity_targets/1
  # DELETE /charity_targets/1.json
  def destroy
    @charity_target.destroy
    respond_to do |format|
      format.html { redirect_to charity_targets_url, notice: 'Charity target was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_charity_target
      @charity_target = CharityTarget.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def charity_target_params
      params.require(:charity_target).permit(:user_id, :type, :name, :latitude, :longitude)
    end
end
