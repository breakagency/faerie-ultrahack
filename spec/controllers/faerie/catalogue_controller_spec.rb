require 'rails_helper'

RSpec.describe CatalogueController, :type => :controller do
  describe "anonymous user" do
    before :each do
      # This simulates an anonymous user
      login_with nil
    end

    it "should be redirected to index" do
      get :set_avatar
      expect( response ).to redirect_to( root_path )
    end

    it "should render index" do
      get :index
      expect( response ).to render_template( :index )
    end
  end

  describe "logged in user" do
    it "should let a user see all the posts" do
      user = create (:user)
      login_with user
      get :render_user, { :format => 'js' }
      expect( response ).to render_template( :render_user )
    end

    it "should let a user see all the posts" do
      user = create (:user)
      login_with user
      get :index, { :total_donated => 111, :format => 'js' }
      expect(user.total_donated).to eq(111)
    end
  end

end