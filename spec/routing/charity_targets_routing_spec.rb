require "rails_helper"

RSpec.describe CharityTargetsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/charity_targets").to route_to("charity_targets#index")
    end

    it "routes to #new" do
      expect(:get => "/charity_targets/new").to route_to("charity_targets#new")
    end

    it "routes to #show" do
      expect(:get => "/charity_targets/1").to route_to("charity_targets#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/charity_targets/1/edit").to route_to("charity_targets#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/charity_targets").to route_to("charity_targets#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/charity_targets/1").to route_to("charity_targets#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/charity_targets/1").to route_to("charity_targets#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/charity_targets/1").to route_to("charity_targets#destroy", :id => "1")
    end

  end
end
