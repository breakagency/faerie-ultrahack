require 'rails_helper'

RSpec.describe "badges/new", type: :view do
  before(:each) do
    assign(:badge, Badge.new(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new badge form" do
    render

    assert_select "form[action=?][method=?]", badges_path, "post" do

      assert_select "input#badge_name[name=?]", "badge[name]"

      assert_select "textarea#badge_description[name=?]", "badge[description]"
    end
  end
end
