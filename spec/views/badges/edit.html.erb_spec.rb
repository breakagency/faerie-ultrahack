require 'rails_helper'

RSpec.describe "badges/edit", type: :view do
  before(:each) do
    @badge = assign(:badge, Badge.create!(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit badge form" do
    render

    assert_select "form[action=?][method=?]", badge_path(@badge), "post" do

      assert_select "input#badge_name[name=?]", "badge[name]"

      assert_select "textarea#badge_description[name=?]", "badge[description]"
    end
  end
end
