require 'rails_helper'

RSpec.describe "charity_targets/show", type: :view do
  before(:each) do
    @charity_target = assign(:charity_target, CharityTarget.create!(
      :user_id => 1,
      :type => "Type",
      :name => "Name",
      :latitude => 1.5,
      :longitude => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Type/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/1.5/)
  end
end
