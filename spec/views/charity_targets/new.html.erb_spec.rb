require 'rails_helper'

RSpec.describe "charity_targets/new", type: :view do
  before(:each) do
    assign(:charity_target, CharityTarget.new(
      :user_id => 1,
      :type => "",
      :name => "MyString",
      :latitude => 1.5,
      :longitude => 1.5
    ))
  end

  it "renders new charity_target form" do
    render

    assert_select "form[action=?][method=?]", charity_targets_path, "post" do

      assert_select "input#charity_target_user_id[name=?]", "charity_target[user_id]"

      assert_select "input#charity_target_type[name=?]", "charity_target[type]"

      assert_select "input#charity_target_name[name=?]", "charity_target[name]"

      assert_select "input#charity_target_latitude[name=?]", "charity_target[latitude]"

      assert_select "input#charity_target_longitude[name=?]", "charity_target[longitude]"
    end
  end
end
