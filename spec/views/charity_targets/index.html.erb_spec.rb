require 'rails_helper'

RSpec.describe "charity_targets/index", type: :view do
  before(:each) do
    assign(:charity_targets, [
      CharityTarget.create!(
        :user_id => 1,
        :type => "Type",
        :name => "Name",
        :latitude => 1.5,
        :longitude => 1.5
      ),
      CharityTarget.create!(
        :user_id => 1,
        :type => "Type",
        :name => "Name",
        :latitude => 1.5,
        :longitude => 1.5
      )
    ])
  end

  it "renders a list of charity_targets" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
